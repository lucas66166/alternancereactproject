// Importe la feuille de style CSS de l'application
import '../style/CardList.css';
// Importe l'image de carte vide à partir du dossier assets
import emptyCard from "../assets/Carte_vide.png";
// Importe les fonctionnalités nécessaires de React et d'autres bibliothèques
import React, { Fragment, useEffect } from 'react';
import axios from 'axios';


// Clé d'API nécessaire pour effectuer des requêtes
const API_KEY = '7edf3e7a9bmshe7a8f0402dd06aep17dadcjsn9af2cecefe47';


function CardList({setCards, filteredCards, setFilteredCards}) {
  
     // Effectue une requête API pour récupérer les cartes au chargement initial
  useEffect(() => {
    fetchCards();
    console.log("La requête à l'API est lancé")
  }, []);

  // Fonction pour récupérer les cartes à partir de l'API
  const fetchCards = async () => {
    // Options de la requête
    const options = {
      method: 'GET',
      url: 'https://omgvamp-hearthstone-v1.p.rapidapi.com/cards/sets/%7BClassic%7D',
      headers: {
        'X-RapidAPI-Key': API_KEY,
        'X-RapidAPI-Host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
      }
    };

    try {
      // Effectue la requête avec axios
      const response = await axios.request(options);
      // Supprime les cartes en double et met à jour l'état local avec les cartes uniques
      const cardsWithImage = removeCardsWithoutImage(response.data);
      setCards(cardsWithImage);
      setFilteredCards(cardsWithImage);
    } catch (error) {
      console.error(error);
    }
  };

  // Fonction pour supprimer les cartes en double
  const removeCardsWithoutImage = cardsData => {
    const cardsWithImage = [];

    for (let card of cardsData) {
      if (card.img !== undefined) {
        cardsWithImage.push(card);
      }
    }

    return cardsWithImage;
  };

  return (
    <Fragment>
      {
        <section>
            <div className='cardList'>
                {filteredCards.map(card => (
            <div key={card.cardId} className='card'>
              <img src={card.img} alt={card.name} />
            </div>
            ))}
        </div>
      </section>
      }
    </Fragment>
  );
}

export default CardList;