// Importe la feuille de style CSS de l'application
import '../style/App.css';
// Importe les composants
import CardList from './CardList';
import SearchBar from "./SearchBar";
import Bienvenue from "./Bienvenue";
// Importe les fonctionnalités nécessaires de React et d'autres bibliothèques
import React, { useState } from 'react';


// Composant principal de l'application
const App = () => {

  // État local pour stocker les cartes et le terme de recherche
  const [cards, setCards] = useState([]);
  const[filteredCards, setFilteredCards] = useState([]);

  // Rendu de l'interface utilisateur
  return (
    <div className='body'> 
      <SearchBar
          cards={cards}
          setFilteredCards={setFilteredCards}
      />
      <CardList
          setCards={setCards}
          filteredCards={filteredCards}
          setFilteredCards={setFilteredCards}
      />
      <Bienvenue/>
    </div>
  );
};

// Exporte le composant principal de l'application
export default App;