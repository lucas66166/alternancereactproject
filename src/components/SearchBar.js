// Importe la feuille de style CSS de l'application
import '../style/SearchBar.css';
// Importe les images
import blizzard_logo from "../assets/blizzard.ico";
import hearthstone_logo from "../assets/logo_hearthstone.ico";
// Importe les fonctionnalités nécessaires de React 
import React, { Fragment } from 'react';

function SearchBar({cards, setFilteredCards}) {
    
  // Gère les changements dans la barre de recherche
  const handleSearch = event => {
    setFilteredCards(filterCards(cards, event.target.value));
  };

  // Filtrage des cartes en fonction du terme de recherche
  const filterCards = (cards, searchTerm) => {
    // SI la barre de recherche est vide
    if (searchTerm === '') {
      // Affiche toute les cartes
      return cards; 
    }

    const filteredCards = [];
    // Variable qui contient la chaine recherché 
    const searchTermLower = searchTerm.toLowerCase();

    for (let card of cards) {
      const cardNameLower = card.name.toLowerCase();

      // Si la carte commence par la chaine de caractère recherché
      if (cardNameLower.startsWith(searchTermLower)) {
        // L'ajoute au début de la liste
        filteredCards.unshift(card); 
      // Si la carte contient la chaine de caractère recherché
      } else if (cardNameLower.includes(searchTermLower)) {
        // L'ajoute à la fin de la liste
        filteredCards.push(card); 
      }
    }

    return filteredCards;
  };
;


  return (
    <Fragment>
      {
        <div className='header'>
            <div className='sameSize'>
                <img src={blizzard_logo} id='blizzard_logo'/>
            </div>
            <img src={hearthstone_logo} />
            <div className='sameSize' id="blockInput">
                <input type="text" placeholder="Search by card name" onChange={handleSearch}/>
            </div>
        </div>
      }
    </Fragment>
  );
}

export default SearchBar;