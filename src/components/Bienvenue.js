// Importe la feuille de style CSS de l'application
import '../style/Bienvenue.css';
// Importe les images
import aubergiste from "../assets/Aubergiste.webm";
import bulleHaut from "../assets/bulle_haut.png";
import bulleMilieu from "../assets/bulle_milieu.png";
import bulleBas from "../assets/bulle_bas.png";
import backcards from "../assets/backcards.avif";

// Importe les fonctionnalités nécessaires de React 
import React, { Fragment } from 'react';
import ReactPlayer from 'react-player';

function Bienvenue() {
  const videoUrl = "../assets/Aubergiste.webm";

  return (
    <Fragment>
      {
      <div className='footer'>
        <img src={backcards}/>
        <video loop muted autoPlay>
          <source src="https://blz-contentstack-assets.akamaized.net/v3/assets/bltc965041283bac56c/bltf54d537b7c061ff9/5f983d63ab20b60bdceae879/homepage_innkeeper_tooltip.webm" />
        </video>
        <div className='bulle'>
          <img src={bulleHaut}/>
          <img src={bulleMilieu} className='midBulle' />
          <p className='text'>Bienvenue dans l'auberge</p>
          <img src={bulleBas} />
        </div>
      </div>
}
    </Fragment>
  );
}

export default Bienvenue;